CREATE TABLE publicaciones
(
    id      VARCHAR(255) PRIMARY KEY,
    user_id VARCHAR(255) NOT NULL,
    publicacion VARCHAR(255) NOT NULL,
    created_at DATE NOT NULL
);

INSERT INTO publicaciones (id, user_id, publicacion, created_at) VALUES(1, '1', 'Hola, esta es mi primer publicación', '2022-03-13 17:00:00.000');
