package com.social.socialjargposts.rabbit;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "socialQueue")
public class RabbitSocialQueueConsumer {

	@RabbitHandler
	public void received(String msg) {
		System.out.println("[socialQueue] received message: " + msg);
	}
}
