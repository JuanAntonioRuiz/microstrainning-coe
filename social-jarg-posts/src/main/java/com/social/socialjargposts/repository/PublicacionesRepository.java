package com.social.socialjargposts.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.social.socialjargposts.entity.Publicaciones;

@Repository
public interface PublicacionesRepository extends CrudRepository<Publicaciones, String> {

	Optional<Publicaciones> findById(String id);
}
