package com.social.socialjargposts.api;

import java.util.List;
import static org.springframework.http.HttpStatus.CREATED;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.social.socialjargposts.entity.Publicaciones;
import com.social.socialjargposts.service.IPublicacionService;

@RestController
@RequestMapping("publicaciones")
public class PublicacionesController {

	@Autowired
	private IPublicacionService publicacionService;
	
	@RequestMapping("/init")
	public String init() {
		return "Hello world, dentro del controller";
	}
	
	@GetMapping("/todas")
	public List<Publicaciones> listar(){
		return (List<Publicaciones>) publicacionService.findAll();
	}
	
	@GetMapping("/post/{id}")
	public Publicaciones deUsuario(@PathVariable String id) {
		return publicacionService.findById(id);
	}
	
	@PostMapping("/publicar")
	@ResponseStatus(CREATED)
	public Publicaciones publicar(@RequestBody Publicaciones publicaciones) {
		return publicacionService.publicar(publicaciones);
	}
}
