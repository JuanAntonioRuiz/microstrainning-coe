package com.social.socialjargposts.service;

import java.util.List;

import com.social.socialjargposts.entity.Publicaciones;

public interface IPublicacionService {

	public List<Publicaciones> findAll();
	public Publicaciones findById(String id);
	public Publicaciones publicar(Publicaciones publicaciones);
}
