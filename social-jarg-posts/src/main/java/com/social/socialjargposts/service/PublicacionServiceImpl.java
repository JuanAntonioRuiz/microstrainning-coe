package com.social.socialjargposts.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.social.socialjargposts.entity.Publicaciones;
import com.social.socialjargposts.repository.PublicacionesRepository;

@Service
public class PublicacionServiceImpl implements IPublicacionService {

	@Autowired
	private PublicacionesRepository publicacionesRepository;
	
	@Override
	@Transactional(readOnly = true)
	public List<Publicaciones> findAll() {
		return (List<Publicaciones>) publicacionesRepository.findAll() ;
	}

	@Override
	@Transactional(readOnly = true)
	public Publicaciones findById(String userId) {
		return publicacionesRepository.findById(userId).orElse(null);
	}

	@Override
	public Publicaciones publicar(Publicaciones publicaciones) {
		Publicaciones publicacion = new Publicaciones(publicaciones.getUserId(), publicaciones.getPublicacion(), publicaciones.getCreatedAt());
		return publicacion;
	}
	
	

}
