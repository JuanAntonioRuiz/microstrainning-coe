CREATE TABLE posts
(
    id      VARCHAR(255) PRIMARY KEY,
    usuario VARCHAR(255) NOT NULL,
    post VARCHAR(255) NOT NULL,
    fecha DATE NOT NULL
);

