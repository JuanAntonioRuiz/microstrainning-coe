package com.social.socialjargserveradmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.codecentric.boot.admin.server.config.EnableAdminServer;

@SpringBootApplication
@EnableAdminServer
public class SocialJargServerAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocialJargServerAdminApplication.class, args);
	}

}
