CREATE TABLE usuarios
(
    id      VARCHAR(255) PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    ap_paterno VARCHAR(255) NOT NULL,
    ap_materno VARCHAR(255) NOT NULL,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    fecha_nacimiento VARCHAR(255) NOT NULL,
    sexo VARCHAR(255) NOT NULL,
    correo VARCHAR(255) NOT NULL
);

INSERT INTO usuarios (id, nombre, ap_paterno, ap_materno, username, password, fecha_nacimiento, sexo, correo) VALUES(1,'Juan Antonio','Ruíz','González','juanruizg4','1234','02/09/1997','M','juan@correo.com.mx');
INSERT INTO usuarios (id, nombre, ap_paterno, ap_materno, username, password, fecha_nacimiento, sexo, correo) VALUES(2,'Jazmin','Sandoval','Hernandez','jazminsandoval','1234','06/11/1998','F','jazmin@correo.com.mx');
INSERT INTO usuarios (id, nombre, ap_paterno, ap_materno, username, password, fecha_nacimiento, sexo, correo) VALUES(3,'Santiago Nicolas','Ruíz','González','santi','1234','02/01/2015','M','santi@correo.com.mx');
