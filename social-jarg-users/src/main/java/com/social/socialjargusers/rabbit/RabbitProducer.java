package com.social.socialjargusers.rabbit;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RabbitProducer {

	@Autowired
	private AmqpTemplate rabbitTemplate;
	
	public void sendSocialQueue() {
		Date date = new Date();
		String dateString = new SimpleDateFormat("YYYY-mm-DD hh:MM:ss").format(date);
        System.out.println("[socialQueue] send msg: " + dateString);  
                 // El primer parámetro es el nombre de la cola que se acaba de definir
        this.rabbitTemplate.convertAndSend("socialQueue", dateString);
	}
}
