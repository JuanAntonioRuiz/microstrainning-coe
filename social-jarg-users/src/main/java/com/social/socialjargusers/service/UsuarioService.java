package com.social.socialjargusers.service;

import java.util.List;

import org.bouncycastle.asn1.x509.sigi.PersonalData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.social.socialjargusers.entity.Usuarios;
import com.social.socialjargusers.modelo.AltaUsuarios;
import com.social.socialjargusers.repository.UsuariosRepository;

@Service
public class UsuarioService implements IUsuarioService{

	@Autowired
	private UsuariosRepository usuariosRepository;
	
	@Override
	@Transactional(readOnly = true)
	public List<Usuarios> findAll() {
		return (List<Usuarios>) usuariosRepository.findAll();
	}

//	@Override
//	public List<Usuarios> obtenerUsuarios() {
//		// TODO Auto-generated method stub
//		return usuariosRepository.;
//	}

	@Override
	@Transactional(readOnly = true)
	public Usuarios findById(String id) {
		return usuariosRepository.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public Usuarios altaUsuario(Usuarios usuarios) {
		Usuarios usuario = new Usuarios(usuarios.getNombre(), usuarios.getApPaterno(), usuarios.getApMaterno(), usuarios.getUsername(), usuarios.getPassword(), usuarios.getFechaNacimiento(), usuarios.getSexo(), usuarios.getCorreo());
//		Usuarios usuario = new Usuarios();
		return usuariosRepository.save(usuario);
	}
	
//	public Boolean crearPersona(PersonaDTO persona) {
//
//	    Persona per = new Persona(persona.getCorreo(), persona.getNombre(), persona.getApellidoPaterno(),
//	        persona.getApellidoMaterno(), persona.getFechaNacimiento(), persona.getContrasena(), persona.getAlias(),
//	        (persona.getRegistroactivo() != null ? persona.getRegistroactivo() : true), persona.getFechaAlta(),
//	        (persona.getFechaModificacion() != null ? persona.getFechaModificacion() : persona.getFechaAlta()),
//	        persona.getIdPersonAlta(),
//	        (persona.getIdPersonaModificacion() != null ? persona.getIdPersonaModificacion()
//	            : persona.getIdPersonAlta()));
//
//	    try {
//	      personaRepositorio.save(per);
//	      return true;
//	    } catch (Exception e) {
//	      System.out.println(e);
//	      return false;
//	    }
//
//	  }

}
