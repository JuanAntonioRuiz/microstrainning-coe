package com.social.socialjargusers.service;

import java.util.List;

import com.social.socialjargusers.entity.Usuarios;
import com.social.socialjargusers.modelo.AltaUsuarios;
import com.social.socialjargusers.repository.UsuariosRepository;

public interface IUsuarioService {

	public List<Usuarios> findAll();
	public Usuarios findById(String id);
	public Usuarios altaUsuario(Usuarios usuarios);
}
