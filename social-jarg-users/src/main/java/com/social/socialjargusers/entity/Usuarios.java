package com.social.socialjargusers.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.social.socialjargusers.modelo.AltaUsuarios;

@Entity
@Table(name = "usuarios")
public class Usuarios implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(updatable = false, columnDefinition = "serial")
	private String id;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "ap_paterno")
	private String apPaterno;
	@Column(name = "ap_materno")
	private String apMaterno;
//	@Column(name = "username")
	private String username;
//	@Column(name = "password")
	private String password;
	@Column(name = "fecha_nacimiento")
	private String fechaNacimiento;
//	@Column(name = "sexo")
	private String sexo;
//	@Column(name = "correo")
	private String correo;
	
	public Usuarios() {
		super();
	}
	public Usuarios(String nombre, String apPaterno, String apMaterno, String username, String password, String fechaNacimiento, String sexo, String correo) {
		super();
		this.nombre = nombre;
		this.apPaterno = apPaterno;
		this.apMaterno = apMaterno;
		this.username = username;
		this.password = password;
		this.fechaNacimiento = fechaNacimiento;
		this.sexo = sexo;
		this.correo = correo;
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApPaterno() {
		return apPaterno;
	}

	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}

	public String getApMaterno() {
		return apMaterno;
	}

	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}
	

//	public static Usuarios from(AltaUsuarios altaUsuarios) {
//		Usuarios usuarios = new Usuarios();
//		usuarios.setNombre(altaUsuarios.getNombre());
//		usuarios.setAp_paterno(altaUsuarios.getAp_paterno());
//		usuarios.setAp_materno(altaUsuarios.getAp_materno());
//		usuarios.setUsername(altaUsuarios.getUsername());
//		usuarios.setPassword(altaUsuarios.getPassword());
//		usuarios.setFecha_nacimiento(altaUsuarios.getFecha_nacimiento());
//		usuarios.setSexo(altaUsuarios.getSexo());
//		usuarios.setCorreo(altaUsuarios.getCorreo());
//		return usuarios;
//	}
//	
//	public AltaUsuarios to() {
//		
//		AltaUsuarios usuarios = new AltaUsuarios();
//		usuarios.setId(id);
//		usuarios.setNombre(nombre);
//		usuarios.setAp_paterno(ap_paterno);
//		usuarios.setAp_materno(ap_materno);
//		usuarios.setUsername(username);
//		usuarios.setPassword(password);
//		usuarios.setFecha_nacimiento(fecha_nacimiento);
//		usuarios.setSexo(sexo);
//		usuarios.setCorreo(correo);
//		return usuarios;
//	}
	
}
	
