package com.social.socialjargusers.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//import com.google.common.base.Optional;
import java.util.Optional;
import com.social.socialjargusers.entity.Usuarios;

@Repository
public interface UsuariosRepository extends CrudRepository<Usuarios, String> {
	Optional<Usuarios> findById(String id);
}
