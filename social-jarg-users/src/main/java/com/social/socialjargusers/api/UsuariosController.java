package com.social.socialjargusers.api;

import static org.springframework.http.HttpStatus.CREATED;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.social.socialjargusers.modelo.AltaUsuarios;
import com.social.socialjargusers.rabbit.RabbitProducer;
import com.social.socialjargusers.service.IUsuarioService;
import com.social.socialjargusers.entity.Usuarios;

import net.bytebuddy.asm.Advice.Return;

@RestController
@RequestMapping("usuarios")
public class UsuariosController {
	
	@Autowired //inyect service class
	private IUsuarioService usuarioService;
	
	@Autowired
	private RabbitProducer rabbitProducer;
	
//	@PostMapping
//	@ResponseStatus(CREATED)
//	public AltaUsuarios crear(@RequestBody AltaUsuarios usuarios) {
	
	@RequestMapping("/init")
	public String init() {
		return "Hello world, dentro del controller";
	}
	
	@GetMapping("/lista")
	public List<Usuarios> listar(){
		
		return (List<Usuarios>) usuarioService.findAll();
	}
	
	@GetMapping("/{id}")
	public Usuarios detalle(@PathVariable String id) {
		return usuarioService.findById(id);
	}
	
	@PostMapping("/alta")
	@ResponseStatus(CREATED)
	public Usuarios altaUsuario(@RequestBody Usuarios usuarios) {
		return usuarioService.altaUsuario(usuarios);
	}
	
	@GetMapping("/sendSocialQueue")
	public Object sendSocialQueue() {
		rabbitProducer.sendSocialQueue();
		return "successful delivery to social queue";
	}
}
