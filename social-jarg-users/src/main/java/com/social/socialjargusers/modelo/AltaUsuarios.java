package com.social.socialjargusers.modelo;

//import org.aspectj.weaver.patterns.ThisOrTargetAnnotationPointcut;

public class AltaUsuarios {
	
	private Integer id;
	private String nombre;
	private String apPaterno;
	private String apMaterno;
	private String username;
	private String password;
	private String fechaNacimiento;
	private String sexo;
	private String correo;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApPaterno() {
		return apPaterno;
	}
	
	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}
	
	public String getApMaterno() {
		return apMaterno;
	}
	
	public void setAp_materno(String apMaterno) {
		this.apMaterno = apMaterno;
	}
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getSexo() {
		return sexo;
	}
	
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getCorreo() {
		return correo;
	}
	
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
}
